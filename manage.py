#!/usr/bin/env python3
import os
from myinvoices import create_app, db
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell
from myinvoices.models import Traders, Invoices, TradeWith

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, MigrateCommand)


def make_shell_context():
    return dict(app=app, db=db, Traders=Traders, Invoices=Invoices,\
                TradeWith=TradeWith)
    
    
@manager.command
def test():
    import unittest
    test = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    
    
manager.add_command('shell', Shell(make_shell_context))
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
