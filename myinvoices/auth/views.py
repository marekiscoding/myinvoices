from flask import render_template, redirect, url_for, flash, request
from flask_login import (login_user, logout_user, login_required, \
                        current_user)
from myinvoices.main.forms import LoginForm, RegisterForm
from myinvoices import bcrypt
from ..models import Traders
from .. import db
from . import auth


@auth.route('/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.interface'))
    form = LoginForm()
    if form.validate_on_submit():
        trader = Traders.query.filter_by(email=form.email.data).first()
        if trader and bcrypt.check_password_hash(trader.password, form.password.data):
            login_user(trader, remember=form.remember.data)
            return redirect(request.args.get('next') or url_for('main.interface'))
    return render_template('auth/login.html', form=form)
    
    
@auth.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.interface'))
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_pwd = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        trader = Traders(email=form.email.data, password=hashed_pwd)
        db.session.add(trader)
        db.session.commit()
        flash('You can now log in.', 'success')
        return redirect(url_for('auth.login'))
    return render_template('register.html', form=form)
        
    
@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You\'re now logged out.', 'info')
    return redirect(url_for('auth.login'))
