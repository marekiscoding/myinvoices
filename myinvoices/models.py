from . import db, login_manager
from flask_login import UserMixin
import time


@login_manager.user_loader
def load_user(trader_id):
    return Traders.query.get(int(trader_id))
    

class Traders(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(40), unique=True, nullable=False)
    password = db.Column(db.String(80), unique=False, nullable=False)
    invoices = db.relationship('Invoices', backref='traders', lazy=True)
    
    
    def __repr__(self):
        return f'Trader("{self.email}")'
        
        
class TradeWith(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    i_trade_with = db.Column(db.String(50), unique=False, nullable=True)
    trader_id = db.Column(db.Integer, db.ForeignKey('traders.id'), nullable=False)
    
        
class Invoices(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    retailer = db.Column(db.String(20), nullable=False)
    upload_date = db.Column(db.String(10), nullable=False, default=time.strftime('%d/%m/%Y'))
    invoice = db.Column(db.String(255), unique=True, nullable=False)
    trader_id = db.Column(db.Integer, db.ForeignKey('traders.id'), nullable=False)
    
    def __repr__(self):
        return f'Invoice("{self.retailer}", "{self.upload_date}")'
        
