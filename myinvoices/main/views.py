from flask import (render_template, session, redirect, url_for, abort, \
                    request, flash, send_from_directory)
from ..models import Traders, Invoices, TradeWith
from flask_login import login_required, current_user
from .forms import UploadInvoice, AddRetailer, SearchField
from werkzeug.utils import secure_filename
import secrets, textract
from manage import app
from .. import db
from . import main
import os, time


ALLOWED_EXTENSIONS = 'pdf'
retailer_confirmed = True
matched_invoices = None
uploading_existing_invoice = False  # needs to be fixed
links = []
prompt = False

def get_retailer(pdf_file):
    pdfFile = textract.process(f"{app.config['UPLOAD_FOLDER']+pdf_file}")
    I_trade_with = TradeWith.query.filter_by(trader_id=current_user.id).all()
    for retailer in I_trade_with:
        if retailer.i_trade_with.lower() in pdfFile.decode('utf-8').lower():
            return retailer.i_trade_with


def search_invoice(key, trader_id):
    invoices = Invoices.query.filter_by(trader_id=trader_id).all()
    found_invoices = []
    for invoice in invoices:
        pdf_invoice = textract.process(f'{app.config["UPLOAD_FOLDER"]+invoice.invoice}')
        if key.lower() in pdf_invoice.decode('utf-8').lower():
            found_invoices.append(invoice.invoice)
    return found_invoices
    
    
def age_of_invoice(trader_id):
    to_delete = []
    invoices = Invoices.query.filter_by(trader_id=trader_id).all()
    for invoice in invoices:
        if int(time.strftime('%Y')) - int(invoice.upload_date[6:]) > 3:
            to_delete.append(invoice.invoice)
            
    os.chdir(f'{app.config["UPLOAD_FOLDER"]}')
    for invoice_to_delete in to_delete:
        Invoices.query.filter_by(invoice=invoice_to_delete, trader_id=trader_id).delete()
        db.session.commit()
        os.remove(invoice_to_delete)
        
    
''' COMMENTED OUT DUE TO FALSE DETECTIONS. TO BE FIXED '''

# ~ def unique_invoice(pdf_file, retailer):     
    # ~ pdfs, decisions = [], []
    # ~ encoded_pdfFile = textract.process(f"{app.config['UPLOAD_FOLDER']+pdf_file}")
    # ~ decoded_pdfFile = encoded_pdfFile.decode('utf-8').lower()
    # ~ invoices = Invoices.query.filter_by(retailer=retailer, trader_id=current_user.id).all()
    # ~ for i in invoices:
        # ~ encoded_pdf = textract.process(f"{app.config['UPLOAD_FOLDER']+pdf_file}")
        # ~ decoded_pdf = encoded_pdf.decode('utf-8').lower()
        # ~ pdfs.append(decoded_pdf)
    
    # ~ for pdf in pdfs:
        # ~ if decoded_pdfFile == pdf:
            # ~ decisions.append('identical invoice found')
        # ~ else:
            # ~ decisions.append('ok')
    
    # ~ return False if 'identical invoice found' in decisions else True
        

@main.route('/about')
def about():
    return render_template('about.html')


@main.route('/logged_in/invoice/<invoice>')
@login_required
def display_pdf(invoice):
    if invoice:
        return send_from_directory('static/pdfs', invoice)

        
@main.route('/logged_in/close_account/<trader_id>')
@login_required
def close_account(trader_id):
    os.chdir(f'{app.config["UPLOAD_FOLDER"]}')
    invoices = Invoices.query.filter_by(trader_id=trader_id).all()
    for invoice in invoices:
        os.remove(invoice.invoice)
    
    Invoices.query.filter_by(trader_id=trader_id).delete()
    TradeWith.query.filter_by(trader_id=trader_id).delete()
    Traders.query.filter_by(id=trader_id).delete()
    db.session.commit()
    flash('Your account has been closed.', 'info')
    return redirect(url_for('auth.login'))
    
        
        
@main.route('/logged_in/delete_retailer/', defaults={'retailer': None}, methods=['GET', 'POST'])
@main.route('/logged_in/delete_retailer/<retailer>', methods=['GET', 'POST'])
@login_required
def delete_retailer(retailer):
    if retailer:
        os.chdir(f'{app.config["UPLOAD_FOLDER"]}')
        for invoice in Invoices.query.filter_by(retailer=retailer, trader_id=current_user.id).all():
            os.remove(invoice.invoice)
        TradeWith.query.filter_by(i_trade_with=retailer, trader_id=current_user.id).delete()
        Invoices.query.filter_by(retailer=retailer, trader_id=current_user.id).delete()
        db.session.commit()
    return redirect(url_for('main.interface'))
        
    
@main.route('/logged_in/', defaults={'retailer': None}, methods=['GET', 'POST'])
@main.route('/logged_in/<retailer>', methods=['GET', 'POST'])
@login_required
def interface(retailer):
    age_of_invoice(current_user.id)
    nudge_is_on = False
    no_invoices_from_this_retailer = None
    retailers = TradeWith.query.filter_by(trader_id=current_user.id).all()
    invoices = Invoices.query.filter_by(retailer=retailer, 
                    trader_id=current_user.id).all()
    
    if retailer and len(invoices) == 0:
        no_invoices_from_this_retailer = True
        
    if retailer:
        global matched_invoices
        # ~ global uploading_existing_invoice
        # ~ uploading_existing_invoice = False
        matched_invoices = None
    form1 = AddRetailer(prefix='form1')
    form2 = UploadInvoice(prefix='form2')
    form3 = SearchField(prefix='form3')
    if retailer is None:
        nudge_is_on = True
    
    if request.method == 'POST' and form1.validate_on_submit():
        new_retailer = TradeWith(i_trade_with=form1.retailer.data,
                                    trader_id=current_user.id)
        db.session.add(new_retailer)
        db.session.commit()
        global retailer_confirmed
        global prompt
        global links
        if len(links) > 0:
            del links[:]
        matched_invoices = None
        prompt = False
        retailer_confirmed = True
        return redirect(url_for('main.interface'))

    elif request.method == 'POST' and form2.validate_on_submit():
        if 'form2-pdf_invoice' not in request.files:
            flash('No file added.')
            return redirect(url_for('interface'))
        file = request.files['form2-pdf_invoice']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename = secrets.token_hex(2) + filename
            invoice = Invoices()
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            invoice.retailer = get_retailer(filename)
            invoice.invoice = filename
            if not invoice.retailer:
                retailer_confirmed = False
                prompt = True
            else:
                prompt = False
            
            matched_invoices = None
            if len(links) > 0:
                del links[:]
            if retailer_confirmed:
                invoice.trader_id = current_user.id
                db.session.add(invoice)
                db.session.commit()
                flash('Invoice added.', 'success')
            else:
                os.chdir(app.config['UPLOAD_FOLDER'])
                os.remove(filename)
                return redirect(url_for('main.interface'))
            return redirect(url_for('main.interface'))
    elif request.method == 'POST' and form3.validate_on_submit():
        matched_invoices = search_invoice(form3.keyword.data, 
                                        trader_id=current_user.id)
        return redirect(url_for('main.interface'))
    
    else:
        return render_template('trader_interface.html', 
            retailers=retailers, invoices=invoices, form1=form1, 
            prompt=prompt, form2=form2, form3=form3, 
            nudge_is_on=nudge_is_on, matched_invoices=matched_invoices, 
            no_invoices_from_this_retailer=no_invoices_from_this_retailer,
            uploading_existing_invoice=uploading_existing_invoice)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    
    
    
    
