from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import (DataRequired, Email, EqualTo, Length, \
                            ValidationError)
from flask_wtf.file import FileField, FileAllowed
from myinvoices.models import Traders
from flask_login import current_user
from flask_wtf import FlaskForm


class LoginForm(FlaskForm):
    email = StringField('', validators=[DataRequired(), Email()])
    password = PasswordField('', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Sign In')
    
    def validate_email(self, field):
        if not Traders.query.filter_by(email=field.data).first():
            raise ValidationError('You don\'t seem to be registered.')
    
            
class RegisterForm(FlaskForm):
    email = StringField('', validators=[DataRequired(), Length(1, 64), \
                                        Email()])
    password = PasswordField('', validators=[DataRequired()])
    confirm_password = PasswordField('', validators=[DataRequired(), \
                    EqualTo('password', message='Passwords must match.')])
    submit = SubmitField('Sign Up')
    
    def validate_email(self, field):
        if Traders.query.filter_by(email=field.data).first():
            raise ValidationError(
                'That email is in use. Please choose a different one.')
                
                
class UploadInvoice(FlaskForm):
    pdf_invoice = FileField(validators=[FileAllowed(['pdf'])])
    submit = SubmitField('Upload')
    
    
class AddRetailer(FlaskForm):
    retailer = StringField('Retailer', validators=[DataRequired(), Length(1, 15)])
    submit = SubmitField('Add')
    

class SearchField(FlaskForm):
    keyword = StringField('', validators=[DataRequired(), Length(1, 15)])
    submit = SubmitField('Search')
